import random as rd
text = "A corporate code of ethics is a statement of business guidelines meant to inform worker behavior and prevent behavior that does not fall in line with the company's mission and greater objectives. Ethical codes, also called codes of conduct, can vary significantly from one company to another. For example, a company that has a goal of preventing harm to the environment might include a list of rules aimed at limiting behaviors that might negatively affect the environment in its code of conduct. Codes of conduct may also include rules aimed at keeping employees in compliance with laws and regulations."

words = text.split()

res = ""
for i in range(500):
    res += " " + words[rd.randint(0, len(words) - 1)]

print(res)
