import React from 'react'
import './styles.scss'
import editIcon from '../../assets/icons/edit.png'
import avatar2 from '../../assets/IMG_0083.JPG'
import icon from '../../assets/icons/face-with-steam-from-nose.png'
import ResourceCard from '../../components/ResourceCard'

export default function Activity({name, email, organize, major}) {
    return (
        <div className="activity">
            <div className="activity__wrapper">
                <img src={avatar2} alt="" className="activity__wrapper__avatar"/>
                <img src={editIcon} alt="" className="activity__wrapper__edit-icon" />
                <p className="activity__wrapper__name" >{name}</p>
                <p className="activity__wrapper__info">{email}</p>
                <p className="activity__wrapper__info">{organize}</p>
                <p className="activity__wrapper__info">{major}</p>
                <div className="activity__wrapper__resources">
                    <p className="activity__wrapper__resources__title">Resources</p>
                    <div className="activity__wrapper__resources__item">
                        <ResourceCard image={avatar2} icon={icon} title="We launch new version of Assed. Check it out" />
                        <ResourceCard image={avatar2} icon={icon} title="We launch new version of Assed. Check it out" />
                        <ResourceCard image={avatar2} icon={icon} title="We launch new version of Assed. Check it out" />
                        <ResourceCard image={avatar2} icon={icon} title="We launch new version of Assed. Check it out" />
                    </div>
                </div>
            </div>
        </div>
    )
}
